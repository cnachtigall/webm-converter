## Webm-Converter
Python command-line tool to convert video files into webm (vp9+opus).

## TODO
* [ ] Change ffmpeg code with ffmpeg pyton plugin
* [ ] Clean up argparse
  * [ ] Update argparse 'ifelse'-part with enums
* [ ] Refactor code